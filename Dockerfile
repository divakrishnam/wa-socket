FROM node:15-alpine

ARG HOST

RUN mkdir -p /usr/src/wa-socket

WORKDIR /usr/src/wa-socket

COPY package*.json ./
RUN npm config set package-lock false
RUN npm install
RUN npm audit fix
RUN echo "export const HOST = $HOST" > config.js
COPY . .

CMD ["npm", "start"]